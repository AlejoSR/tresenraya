﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TresEnRaya
{
    public partial class MainPage : ContentPage
    {
        private UnJugadorModel UnJugador;
        private DosJugadoresModel DosJugadores;

        public MainPage()
        {
            InitializeComponent();
            UnJugador = new UnJugadorModel();
            DosJugadores = new DosJugadoresModel();

            this.BindingContext = UnJugador;
            this.BindingContext = DosJugadores;
        }

        private async void BotonUnJugador(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaUnJugador());

            PaginaUnJugador PaginaJuego = new PaginaUnJugador();
        }

        private async void BotonDosJugadores(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaDosJugadores());

            PaginaDosJugadores PaginaJuego = new PaginaDosJugadores();
        }
    }
}
