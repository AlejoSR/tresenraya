﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TresEnRaya
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaginaUnJugador : ContentPage
	{
        private UnJugadorModel UnJugador;
		public PaginaUnJugador ()
		{
			InitializeComponent ();

            UnJugador = new UnJugadorModel
            {
                Puntos = 0,
                Tecla1 = "*",
                Tecla2 = "*",
                Tecla3 = "*",
                Tecla4 = "*",
                Tecla5 = "*",
                Tecla6 = "*",
                Tecla7 = "*",
                Tecla8 = "*",
                Tecla9 = "*",
            };

            this.BindingContext = UnJugador;
		}
	}
}