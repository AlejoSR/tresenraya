﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace TresEnRaya
{
    class DosJugadoresModel : INotifyPropertyChanged
    {
        public int _PuntosJ1, _PuntosJ2;
        public string _Tecla1, _Tecla2, _Tecla3, _Tecla4, _Tecla5, _Tecla6, _Tecla7, _Tecla8, _Tecla9;

        public Command CommandTecla1 { get; set; }
        public Command CommandTecla2 { get; set; }
        public Command CommandTecla3 { get; set; }
        public Command CommandTecla4 { get; set; }
        public Command CommandTecla5 { get; set; }
        public Command CommandTecla6 { get; set; }
        public Command CommandTecla7 { get; set; }
        public Command CommandTecla8 { get; set; }
        public Command CommandTecla9 { get; set; }
        public Command CommandReiniciarTablero { get; set; }
        public Command CommandReiniciarPuntos { get; set; }

        public DosJugadoresModel()
        {
            CommandTecla1 = new Command(JuegaTecla1);
            CommandTecla2 = new Command(JuegaTecla2);
            CommandTecla3 = new Command(JuegaTecla3);
            CommandTecla4 = new Command(JuegaTecla4);
            CommandTecla5 = new Command(JuegaTecla5);
            CommandTecla6 = new Command(JuegaTecla6);
            CommandTecla7 = new Command(JuegaTecla7);
            CommandTecla8 = new Command(JuegaTecla8);
            CommandTecla9 = new Command(JuegaTecla9);
            CommandReiniciarTablero = new Command(ReiniciarTablero);
            CommandReiniciarPuntos = new Command(ReiniciarPuntos);
        }

        public int PuntosJ1 { get { return _PuntosJ1; } set { _PuntosJ1 = value; OnPropertyChanged(); } }
        public int PuntosJ2 { get { return _PuntosJ2; } set { _PuntosJ2 = value; OnPropertyChanged(); } }

        public string Tecla1 { get { return _Tecla1; } set { _Tecla1 = value; OnPropertyChanged(); } }
        public string Tecla2 { get { return _Tecla2; } set { _Tecla2 = value; OnPropertyChanged(); } }
        public string Tecla3 { get { return _Tecla3; } set { _Tecla3 = value; OnPropertyChanged(); } }
        public string Tecla4 { get { return _Tecla4; } set { _Tecla4 = value; OnPropertyChanged(); } }
        public string Tecla5 { get { return _Tecla5; } set { _Tecla5 = value; OnPropertyChanged(); } }
        public string Tecla6 { get { return _Tecla6; } set { _Tecla6 = value; OnPropertyChanged(); } }
        public string Tecla7 { get { return _Tecla7; } set { _Tecla7 = value; OnPropertyChanged(); } }
        public string Tecla8 { get { return _Tecla8; } set { _Tecla8 = value; OnPropertyChanged(); } }
        public string Tecla9 { get { return _Tecla9; } set { _Tecla9 = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Habilitado = true;
        public int Turno = 1;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void GanaJ1()
        {
            PuntosJ1 = PuntosJ1 + 100;
            Habilitado = false;
            Turno = 1;
        }

        public void GanaJ2()
        {
            PuntosJ2 = PuntosJ2 + 100;
            Habilitado = false;
            Turno = 2;
        }

        public void ReiniciarTablero()
        {
            Tecla1 = "*";
            Tecla2 = "*";
            Tecla3 = "*";
            Tecla4 = "*";
            Tecla5 = "*";
            Tecla6 = "*";
            Tecla7 = "*";
            Tecla8 = "*";
            Tecla9 = "*";
            Habilitado = true;
        }

        public void ReiniciarPuntos()
        {
            PuntosJ1 = 0;
            PuntosJ2 = 0;
        }

        public int Conteo()
        {
            int libres = 0;

            if (Tecla1 == "*")
            {
                libres++;
            }
            if (Tecla2 == "*")
            {
                libres++;
            }
            if (Tecla3 == "*")
            {
                libres++;
            }
            if (Tecla4 == "*")
            {
                libres++;
            }
            if (Tecla5 == "*")
            {
                libres++;
            }
            if (Tecla6 == "*")
            {
                libres++;
            }
            if (Tecla7 == "*")
            {
                libres++;
            }
            if (Tecla8 == "*")
            {
                libres++;
            }
            if (Tecla9 == "*")
            {
                libres++;
            }
            return libres;
        }

        //----------------------- JUEGA TECLAS -----------------------//

        public void JuegaTecla1()
        {
            int libres = Conteo();
            if (Tecla1 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla1 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla1 = "O";
                    Turno = 1;
                }
                ValidarTecla1();
                
            }
        }

        public void JuegaTecla2()
        {
            int libres = Conteo();
            if (Tecla2 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla2 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla2 = "O";
                    Turno = 1;
                }
                ValidarTecla2();
            }
        }

        public void JuegaTecla3()
        {
            int libres = Conteo();
            if (Tecla3 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla3 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla3 = "O";
                    Turno = 1;
                }
                ValidarTecla3();
            }
        }

        public void JuegaTecla4()
        {
            int libres = Conteo();
            if (Tecla4 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla4 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla4 = "O";
                    Turno = 1;
                }
                ValidarTecla4();
            }
        }

        public void JuegaTecla5()
        {
            int libres = Conteo();
            if (Tecla5 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla5 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla5 = "O";
                    Turno = 1;
                }
                ValidarTecla5();
            }
        }

        public void JuegaTecla6()
        {
            int libres = Conteo();
            if (Tecla6 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla6 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla6 = "O";
                    Turno = 1;
                }
                ValidarTecla6();
            }
        }

        public void JuegaTecla7()
        {
            int libres = Conteo();
            if (Tecla7 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla7 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla7 = "O";
                    Turno = 1;
                }
                ValidarTecla7();
            }
        }

        public void JuegaTecla8()
        {
            int libres = Conteo();
            if (Tecla8 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla8 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla8 = "O";
                    Turno = 1;
                }
                ValidarTecla8();
            }
        }

        public void JuegaTecla9()
        {
            int libres = Conteo();
            if (Tecla9 == "*" && Habilitado == true)
            {
                if (Turno == 1)
                {
                    Tecla9 = "X";
                    Turno = 2;
                }
                else
                {
                    Tecla9 = "O";
                    Turno = 1;
                }
                ValidarTecla9();
            }
        }

        //----------------------- VALIDACIONES -----------------------//

        public void ValidarTecla1()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla2()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla3()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla4()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla5()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla6()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla7()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla8()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O"))
            {
                GanaJ2();
            }
        }

        public void ValidarTecla9()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X"))
            {
                GanaJ1();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O"))
            {
                GanaJ2();
            }
        }
    }
}
