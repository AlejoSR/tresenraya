﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace TresEnRaya
{
    class UnJugadorModel : INotifyPropertyChanged
    {
        public int _Puntos;
        public string _Tecla1, _Tecla2, _Tecla3, _Tecla4, _Tecla5, _Tecla6, _Tecla7, _Tecla8, _Tecla9;

        public Command CommandTecla1 { get; set; }
        public Command CommandTecla2 { get; set; }
        public Command CommandTecla3 { get; set; }
        public Command CommandTecla4 { get; set; }
        public Command CommandTecla5 { get; set; }
        public Command CommandTecla6 { get; set; }
        public Command CommandTecla7 { get; set; }
        public Command CommandTecla8 { get; set; }
        public Command CommandTecla9 { get; set; }
        public Command CommandReiniciarTablero { get; set; }
        public Command CommandReiniciarPuntos { get; set; }

        public UnJugadorModel()
        {
            CommandTecla1 = new Command(JuegaTecla1);
            CommandTecla2 = new Command(JuegaTecla2);
            CommandTecla3 = new Command(JuegaTecla3);
            CommandTecla4 = new Command(JuegaTecla4);
            CommandTecla5 = new Command(JuegaTecla5);
            CommandTecla6 = new Command(JuegaTecla6);
            CommandTecla7 = new Command(JuegaTecla7);
            CommandTecla8 = new Command(JuegaTecla8);
            CommandTecla9 = new Command(JuegaTecla9);
            CommandReiniciarTablero = new Command(ReiniciarTablero);
            CommandReiniciarPuntos = new Command(ReiniciarPuntos);
        }

        public int Puntos { get { return _Puntos; } set { _Puntos = value; OnPropertyChanged(); } }

        public string Tecla1 { get { return _Tecla1; } set { _Tecla1 = value; OnPropertyChanged(); } }
        public string Tecla2 { get { return _Tecla2; } set { _Tecla2 = value; OnPropertyChanged(); } }
        public string Tecla3 { get { return _Tecla3; } set { _Tecla3 = value; OnPropertyChanged(); } }
        public string Tecla4 { get { return _Tecla4; } set { _Tecla4 = value; OnPropertyChanged(); } }
        public string Tecla5 { get { return _Tecla5; } set { _Tecla5 = value; OnPropertyChanged(); } }
        public string Tecla6 { get { return _Tecla6; } set { _Tecla6 = value; OnPropertyChanged(); } }
        public string Tecla7 { get { return _Tecla7; } set { _Tecla7 = value; OnPropertyChanged(); } }
        public string Tecla8 { get { return _Tecla8; } set { _Tecla8 = value; OnPropertyChanged(); } }
        public string Tecla9 { get { return _Tecla9; } set { _Tecla9 = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Habilitado = true;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Ganar()
        {
            Puntos = Puntos + 100;
            Habilitado = false;

        }

        public void Perder()
        {
            Puntos = Puntos - 100;
            Habilitado = false;
        }

        public void ReiniciarTablero()
        {
            Tecla1 = "*";
            Tecla2 = "*";
            Tecla3 = "*";
            Tecla4 = "*";
            Tecla5 = "*";
            Tecla6 = "*";
            Tecla7 = "*";
            Tecla8 = "*";
            Tecla9 = "*";
            Habilitado = true;
        }

        public void ReiniciarPuntos()
        {
            Puntos = 0;
        }

        public int Conteo()
        {
            int libres = 0;

            if (Tecla1 == "*")
            {
                libres++;
            }
            if (Tecla2 == "*")
            {
                libres++;
            }
            if (Tecla3 == "*")
            {
                libres++;
            }
            if (Tecla4 == "*")
            {
                libres++;
            }
            if (Tecla5 == "*")
            {
                libres++;
            }
            if (Tecla6 == "*")
            {
                libres++;
            }
            if (Tecla7 == "*")
            {
                libres++;
            }
            if (Tecla8 == "*")
            {
                libres++;
            }
            if (Tecla9 == "*")
            {
                libres++;
            }
            return libres;
        }

        public void TurnoMaquina()
        {
            int libres = Conteo();
            if (libres > 1 && libres < 9 && Habilitado == true)
            {
                Random rnd = new Random();
                int aleatorio = rnd.Next(1, 9);

                switch (aleatorio)
                {
                    case 1:
                        if (Tecla1 == "*")
                        {
                            Tecla1 = "O";
                            ValidarTecla1();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 2:
                        if (Tecla2 == "*")
                        {
                            Tecla2 = "O";
                            ValidarTecla2();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 3:
                        if (Tecla3 == "*")
                        {
                            Tecla3 = "O";
                            ValidarTecla3();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 4:
                        if (Tecla4 == "*")
                        {
                            Tecla4 = "O";
                            ValidarTecla4();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 5:
                        if (Tecla5 == "*")
                        {
                            Tecla5 = "O";
                            ValidarTecla5();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 6:
                        if (Tecla6 == "*")
                        {
                            Tecla6 = "O";
                            ValidarTecla6();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 7:
                        if (Tecla7 == "*")
                        {
                            Tecla7 = "O";
                            ValidarTecla7();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 8:
                        if (Tecla8 == "*")
                        {
                            Tecla8 = "O";
                            ValidarTecla8();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;

                    case 9:
                        if (Tecla9 == "*")
                        {
                            Tecla9 = "O";
                            ValidarTecla9();
                        }
                        else
                        {
                            TurnoMaquina();
                        }
                        break;
                }
            }
        }

        //----------------------- JUEGA TECLAS -----------------------//

        public void JuegaTecla1()
        {
            int libres = Conteo();
            if (Tecla1 == "*" && Habilitado == true)
            {
                Tecla1 = "X";
                ValidarTecla1();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla2()
        {
            int libres = Conteo();
            if (Tecla2 == "*" && Habilitado == true)
            {
                Tecla2 = "X";
                ValidarTecla2();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla3()
        {
            int libres = Conteo();
            if (Tecla3 == "*" && Habilitado == true)
            {
                Tecla3 = "X";
                ValidarTecla3();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla4()
        {
            int libres = Conteo();
            if (Tecla4 == "*" && Habilitado == true)
            {
                Tecla4 = "X";
                ValidarTecla4();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla5()
        {
            int libres = Conteo();
            if (Tecla5 == "*" && Habilitado == true)
            {
                Tecla5 = "X";
                ValidarTecla5();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla6()
        {
            int libres = Conteo();
            if (Tecla6 == "*" && Habilitado == true)
            {
                Tecla6 = "X";
                ValidarTecla6();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla7()
        {
            int libres = Conteo();
            if (Tecla7 == "*" && Habilitado == true)
            {
                Tecla7 = "X";
                ValidarTecla7();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla8()
        {
            int libres = Conteo();
            if (Tecla8 == "*" && Habilitado == true)
            {
                Tecla8 = "X";
                ValidarTecla8();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        public void JuegaTecla9()
        {
            int libres = Conteo();
            if (Tecla9 == "*" && Habilitado == true)
            {
                Tecla9 = "X";
                ValidarTecla9();
                if (libres > 1 && Habilitado == true)
                {
                    TurnoMaquina();
                }
            }
        }

        //----------------------- VALIDACIONES -----------------------//

        public void ValidarTecla1()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X"))
            {
                Ganar();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla2()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X"))
            {
                Ganar();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla3()
        {
            if ((Tecla1 == "X" && Tecla2 == "X" && Tecla3 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                Ganar();
            }

            if ((Tecla1 == "O" && Tecla2 == "O" && Tecla3 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla4()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X"))
            {
                Ganar();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla5()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                Ganar();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla6()
        {
            if ((Tecla4 == "X" && Tecla5 == "X" && Tecla6 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X"))
            {
                Ganar();
            }

            if ((Tecla4 == "O" && Tecla5 == "O" && Tecla6 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla7()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla1 == "X" && Tecla4 == "X" && Tecla7 == "X") || (Tecla3 == "X" && Tecla5 == "X" && Tecla7 == "X"))
            {
                Ganar();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla1 == "O" && Tecla4 == "O" && Tecla7 == "O") || (Tecla3 == "O" && Tecla5 == "O" && Tecla7 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla8()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla2 == "X" && Tecla5 == "X" && Tecla8 == "X"))
            {
                Ganar();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla2 == "O" && Tecla5 == "O" && Tecla8 == "O"))
            {
                Perder();
            }
        }

        public void ValidarTecla9()
        {
            if ((Tecla7 == "X" && Tecla8 == "X" && Tecla9 == "X") || (Tecla3 == "X" && Tecla6 == "X" && Tecla9 == "X") || (Tecla1 == "X" && Tecla5 == "X" && Tecla9 == "X"))
            {
                Ganar();
            }

            if ((Tecla7 == "O" && Tecla8 == "O" && Tecla9 == "O") || (Tecla3 == "O" && Tecla6 == "O" && Tecla9 == "O") || (Tecla1 == "O" && Tecla5 == "O" && Tecla9 == "O"))
            {
                Perder();
            }
        }
    }
}
